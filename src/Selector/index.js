import React, { Component, Fragment } from "react";

import Select from "react-select";
import { colourOptions } from "../Data";
import "../Data/some.css"

// const Checkbox = (props) => <input type="checkbox" {...props} />;

const colourStyles = {
  

    control: styles => ({ ...styles, backgroundColor: 'white'}),
    option: (styles, { data, isDisabled, isFocused, isSelected }) => {
    //   const color = chroma(data.color);
      return {
        ...styles,
        // backgroundColor: isDisabled ? 'red' : "blue",
        // color: '#FFF',
        cursor: isDisabled ? 'not-allowed' : 'default',
      };
    },

  };
  

export default class SingleSelect extends Component {
   


    state = {
    isClearable: true,
    isDisabled: false,
    isLoading: false,
    isRtl: false,
    isSearchable: true
  };

  toggleClearable = () =>
    this.setState((state) => ({ isClearable: !state.isClearable }));
  toggleDisabled = () =>
    this.setState((state) => ({ isDisabled: !state.isDisabled }));
  toggleLoading = () =>
    this.setState((state) => ({ isLoading: !state.isLoading }));
  toggleRtl = () => this.setState((state) => ({ isRtl: !state.isRtl }));
  toggleSearchable = () =>
    this.setState((state) => ({ isSearchable: !state.isSearchable }));
  render() {
    const {
      isClearable,
      isSearchable,
      isDisabled,
      isLoading,
      isRtl
    } = this.state;
    return (
      <Fragment>
        <Select
          className="basic-single"
          classNamePrefix="my-custom-react-select"
        //   defaultValue={colourOptions[0]}
          isDisabled={isDisabled}
          isLoading={isLoading}
          isClearable={isClearable}
          isRtl={isRtl}
          isSearchable={isSearchable}
          name="color"
          options={colourOptions}
          styles={colourStyles}
        
        // styles={{
        //     // Fixes the overlapping problem of the component
        //     options: provided => ({ ...provided, zIndex: 9999 })
        //   }}

        />
      </Fragment>
    );
  }
}
