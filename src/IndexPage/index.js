import React, { Component } from "react"
import { Route, withRouter } from "react-router-dom"
import qs from "qs"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import {
  faList,
  faTh,
  faFilter,
  faDollarSign,
  faSortAmountDown,
  faSortAmountUp, faEye , faRandom
} from "@fortawesome/free-solid-svg-icons"
import {
  CardGrid,
  Controls,
  Toggle,
  NoResults
} from "./Components"

import Card from "./Card"
import IconSetPage from "../IconSetPage"
import { Contents } from "../BaseComponents"
import icons from "../IconComponents"

const defaultState = {
  filter: "",
  display: "grid",
  sort: "random",
  show : false
}

const sortByIconCount = (iconKeys, sort ,show) => {
  if (sort === "ascending") {
    return [...iconKeys].sort((a, b) => {
      if (icons[a].length < icons[b].length) return -1
      else if (icons[b].length < icons[a].length) return 1
      else return 0
    })
  }
  else  if (sort === "random") {

    return [...iconKeys].sort((a, b) => {
      // if (icons[a].length < icons[b].length) return 0
      // else if (icons[b].length < icons[a].length) return 0
    var noo = Math.floor(Math.random() * 10);

      if(show === "true")
      return 0;
      else if(noo > 4 ) return 1;
      else if (noo <= 4 ) return -1;

      else return 0
    })
  } else {
    return [...iconKeys].sort((a, b) => {
      if (icons[a].length > icons[b].length) return -1
      else if (icons[b].length > icons[a].length) return 1
      else return 0
    })
  }
}

class IndexPage extends Component {
  constructor(props) {
    super(props);
    this.state = {show: "false",
    colourOptions : [
      { k : 0 , value: 'ocean', label: 'Computer Science', color: '#00B8D9',isDisabled: false  },
      { k : 1 ,  value: 'blue', label: 'Information Technology', color: '#0052CC', isDisabled: false },
      { k : 2 ,  value: 'purple', label: 'Electronics and Telecommunication', color: '#5243AA', isDisabled: false },
      { k : 3 ,  value: 'red', label: 'Electronics', color: '#FF5630', isDisabled: false  },
      { k : 4 ,  value: 'orange', label: 'Electronics Design Technology', color: '#FF8B00' , isDisabled: false},
      { k : 5 ,  value: 'yellow', label: 'Electrical', color: '#FFC400', isDisabled: false },
      { k : 6 ,  value: 'green', label: 'Mechanical', color: '#36B37E' , isDisabled: false},
      { k : 7 ,  value: 'forest', label: 'Industrial', color: '#00875A', isDisabled: false },
      { k : 8 ,  value: 'slate', label: 'Civil', color: '#253858' , isDisabled: false},
      { k : 9 ,  value: 'silver', label: 'MBA', color: '#666666', isDisabled: false },
      { k : 10 ,  value: 'silver', label: 'MCA', color: '#666666', isDisabled: false },
    ]
  
  };
  }


  static propTypes = {}




  updateQueryParam = obj => {
    this.props.history.push({
      search: `?${qs.stringify({
        ...qs.parse(this.props.location.search.replace("?", "")),
        ...obj
      })}`
    })


  }

  navigate = set => {
    // this.props.history.push({
    //   pathname: `/${set.replace(/\s/g, "-")}`,
    //   search: this.props.location.search
    // })
    // console.log(set);
  }

  toggleShow = set =>{
    if(this.state.show === "false")
    this.setState({ show : "true"});
    else
    this.setState({ show : "false"});
    // this.setState({ colourOptions[0].isDisabled : true});

  }
  




  render() {
    const focusedSet = this.props.location.pathname.split(/\//g)[1]

    const queryParamState = {
      ...defaultState,
      ...qs.parse(this.props.location.search.replace("?", ""))
    }



    const visibleIconSets = sortByIconCount(
      Object.keys(icons),
      queryParamState.sort,this.state.show
    ).filter(set =>
      queryParamState.filter
        ? set.match(new RegExp("^" + queryParamState.filter))
        : true
    )
    
    return (
      <div>
        <Contents>
          <Controls>
            <div>
              <Toggle
                active={queryParamState.sort === "ascending"}
                onClick={() => this.updateQueryParam({ sort: "ascending" })}
              >
                <FontAwesomeIcon icon={faSortAmountUp} />
              </Toggle> 
              <Toggle
                active={queryParamState.sort === "descending"}
                onClick={() => this.updateQueryParam({ sort: "descending" })}
              >
                <FontAwesomeIcon icon={faSortAmountDown} />
              </Toggle>
              <Toggle
                active={queryParamState.sort === "random"}
                onClick={() => this.updateQueryParam({ sort: "random"  })}
              >
                <FontAwesomeIcon icon={faRandom} />
              </Toggle>
            </div>
            
              
            <div>
              <Toggle
                active={queryParamState.display === "list"}
                onClick={() =>{ 
                  
                  if (queryParamState.display === "grid")
                    return this.updateQueryParam({ display: "list" })
                  else
                    return this.updateQueryParam({ display: "grid" })
              }
              }
              >
                <FontAwesomeIcon icon={
                  
                  (queryParamState.display === "list")?faList:faTh
                } />
              </Toggle>

              <Toggle
                active={this.state.show === "true"}

                onClick={() => this.toggleShow({ display: "list" })}
              >
                <FontAwesomeIcon icon={faEye} />
              </Toggle>
            </div>
          </Controls>
          {visibleIconSets.length === 0 ? (
            <NoResults>No Results Found</NoResults>
          ) : (
            <CardGrid
              display={queryParamState.display}
              ref={el => (this.cardGrid = el)}
            >
              {/* {visibleIconSets.map(set => {
                console.log(set);
                if (set === focusedSet) return <li key={set} />
                return (
                  <Card
                    key={set}
                    setKey={set}
                    icons={icons[set]}
                    iconCount={icons[set].length}
                    navigate={this.navigate}
                  />
                )
              })} */}


              {
              
              visibleIconSets.map(set => {
                if (set === focusedSet) return <li key={set} />
                return (
                  <Card
                    key={set}
                    setKey={set}
                    icons={icons[set]}
                    iconCount={icons[set].length}
                    navigate={this.navigate}
                    show = {this.state.show}
                  />
                )
              })

              // [1,2,3,4,5].map(num =>{

              //   return (
              //         <Card
              //           key={num}
              //           setKey={num}
              //           icons={icons[num]}
              //           iconCount={icons[num].length}
              //           navigate={this.navigate}
              //         />
              //       )

              // })
              
              
              
              }


            </CardGrid>
          )}
        </Contents>
        {/* <Route path="/:set/:focusedIcon?" component={IconSetPage} /> */}
      </div>
    )
  }
}

export default withRouter(IndexPage)
